/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"Iosevka Fixed Slab:size=16"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const char col_black[]       = "#1d2021";
static const char col_lightgray[]   = "#a89984";
static const char col_aqua[]        = "#89b594";
static const char col_orange[]      = "#f7a583";
static const char col_purple[]      = "#d3869b";
static const char col_white[]       = "#ebdbb2";
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { col_lightgray, col_black },
	[SchemeSel] = { col_black, col_aqua },
	[SchemeSelHighlight] = { col_black, col_orange },
	[SchemeNormHighlight] = { col_black, col_lightgray },
	[SchemeOut] = { col_white, col_aqua },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
