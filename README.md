Swy7ch' fork of dmenu
=====================

This is my fork of suckless' most famous tool, [dmenu](https://tools.suckless.org/dmenu/). The repo is here because I wanted to config dmenu instead of passing it parameters. The only tweaks are the font and color management, and a fuzzyfinder behaviour. Enjoy!

---

dmenu - dynamic menu
====================
dmenu is an efficient dynamic menu for X.


Requirements
------------
In order to build dmenu you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (dmenu is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dmenu
(if necessary as root):

    make clean install


Running dmenu
-------------
See the man page for details.
